#!/usr/bin/python
# -*- coding: utf-8 -*-
from math import sqrt
import socket #utilidades de red y conexion
#declaramos las variables
ipServidor = "192.168.43.108" #es lo mismo que "localhost" o "0.0.0.0"
puertoServidor = 9798


#socket.AF_INET para indicar que utilizaremos Ipv4d
#socket.SOCK_STREAM para utilizar TCP/IP (no udp)
#Estos protocolos deben ser los mismos que en el servidor
cliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
cliente.connect((ipServidor, puertoServidor))
print("Conectado con el servidor ---> %s:%s" %(ipServidor, puertoServidor))

while True:
    print("")
    print("Resolver ecuaciones de segundo grado de la forma ax^2+bx+c")
    print("")
    print("introduzca el valor del coeficiente de la variable a cuadratica ")
    a = int(input("> "))
    print("")
    print("introduzca el valor del coeficiente de la variable b lineal")
    b = int(input("> "))
    print("")
    print("introduzca el valor del coeficiente de la variable c termino independiente")
    c = int(input("> "))
    
    if((b*b)-4*a*c) < 0:
        msg = "La solucion implica numeros imaginarios"
        
    else:
        x1 = (-b+sqrt(b**2-(4*a*c)))/(2*a)
        x2 = (-b-sqrt(b**2-(4*a*c)))/(2*a)
        msg = str(x1) +", " + str(x2) 
        
        
    print("")
    
    cliente.send(msg.encode('UTF-8'))
    respuesta = cliente.recv(4096)
    print('respuesta de servidor: {}'.format(respuesta.decode('utf-8')))
    #print(respuesta)
    if respuesta == "salir":
        break;

print("------- CONEXIÓN CERRADA ---------")
cliente.close()
