import Adafruit_DHT  
import time
import RPi.GPIO as GPIO



while True:
  sensor = Adafruit_DHT.DHT11 #Cambia por DHT22 y si usas dicho sensor
  pin = 3
  pin_led= 19 #Pin en la raspberry donde conectamos el sensor
  humedad, temperatura = Adafruit_DHT.read_retry(sensor, pin)
  
  GPIO.setmode(GPIO.BCM)
  GPIO.setup(pin_led, GPIO.OUT)

  print ('Temperatura: ' , temperatura)
 
  time.sleep(1) #Cada segundo se evalúa el sensor
  
  if(temperatura > 30.0):
      
      GPIO.output(pin_led, GPIO.HIGH)
  else:
      GPIO.output(pin_led, GPIO.LOW)
      GPIO.cleanup()