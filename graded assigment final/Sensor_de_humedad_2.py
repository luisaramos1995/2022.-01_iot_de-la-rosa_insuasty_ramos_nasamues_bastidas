from gpiozero import DigitalInputDevice
import time
from gpiozero import AngularServo
from time import sleep
servo = AngularServo(21, min_pulse_width=0.0006, max_pulse_width=0.0023)
 
d0_input = DigitalInputDevice(4)
 
while True:
    if (not d0_input.value):
        print('Humedad alcanzada!!!')
    else:
        print('Tu planta necesita agua')
        sleep(2)
        servo.angle = 30
        sleep(2)
        servo.angle = -90
        