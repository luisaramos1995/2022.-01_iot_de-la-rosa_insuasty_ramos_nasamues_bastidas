import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import RPi.GPIO as GPIO
import random
import datetime
import time
import Adafruit_DHT  

TRIG = 18 #Variable que contiene el GPIO al cual conectamos la señal TRIG del sensor
ECHO = 24 #Variable que contiene el GPIO al cual conectamos la señal ECHO del sensor
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)     #Establecemos el modo según el cual nos refiriremos a los GPIO de nuestra RPi            
GPIO.setup(TRIG, GPIO.OUT) #Configuramos el pin TRIG como una salida 
GPIO.setup(ECHO, GPIO.IN)


def ultrasonico_sensor():
    
    # Ponemos en bajo el pin TRIG y después esperamos 0.5 seg para que el transductor se estabilice
    GPIO.output(TRIG, GPIO.LOW)
    time.sleep(1)

    #Ponemos en alto el pin TRIG esperamos 10 uS antes de ponerlo en bajo
    GPIO.output(TRIG, GPIO.HIGH)
    time.sleep(0.00001)
    GPIO.output(TRIG, GPIO.LOW)

    # En este momento el sensor envía 8 pulsos ultrasónicos de 40kHz y coloca su pin ECHO en alto
    # Debemos detectar dicho evento para iniciar la medición del tiempo
    
    while True:
        pulso_inicio = time.time()
        if GPIO.input(ECHO) == GPIO.HIGH:
            break

    # El pin ECHO se mantendrá en HIGH hasta recibir el eco rebotado por el obstáculo. 
    # En ese momento el sensor pondrá el pin ECHO en bajo.
# Prodedemos a detectar dicho evento para terminar la medición del tiempo
    
    while True:
        pulso_fin = time.time()
        if GPIO.input(ECHO) == GPIO.LOW:
            break

    # Tiempo medido en segundos
    duracion = pulso_fin - pulso_inicio
    distancia = (34300 * duracion) / 2
    
    distancia_ultrasonico = distancia
    

    #Obtenemos la distancia considerando que la señal recorre dos veces la distancia a medir y que la velocidad del sonido es 343m/s
    

    # Imprimimos resultado
#     print( "Distancia: %.2f cm" % distancia)
    return distancia
    
def Temperatura_sensor():
    sensor = Adafruit_DHT.DHT11 #Cambia por DHT22 y si usas dicho sensor
    pin = 3
#     pin_led= 19 #Pin en la raspberry donde conectamos el sensor
    humedad, temperatura_value = Adafruit_DHT.read_retry(sensor, pin)
    temperatura = temperatura_value

#     GPIO.setmode(GPIO.BCM)
#     GPIO.setup(pin_led, GPIO.OUT)

#     print ('Temperatura: ' , temperatura)

    time.sleep(1) #Cada segundo se evalúa el sensor
    return temperatura_value

def generate_time(initial_time):
    final_time= datetime.datetime.now()
    delta = final_time - initial_time
    return delta.total_seconds()

initial_time= datetime.datetime.now()

try:
    app = firebase_admin.get_app()
except ValueError as e:
    # Fetch the service account key JSON file contents
    cred = credentials.Certificate('admin.json')
    # Initialize the app with a service account, granting admin privileges
    firebase_admin.initialize_app(cred, {'databaseURL': 'https://fir-test-fa23c-default-rtdb.firebaseio.com'})

# Create estructure
ref = db.reference("sensors/")
ref.set({'ultrasonico':{'data': 0.0,'time': 0.0 }, 'Temperatura':{'data': 0.0,'time': 0.0 }})

# update sensors
ref = db.reference('sensors')

trigger = True

while True:
   
    try:
        ultrasonico = ultrasonico_sensor()
        # Sensor 1
        time1 =  generate_time(initial_time)
        
        # Sensor 2
        Temperatura = Temperatura_sensor()
        time2 =  generate_time(initial_time)
        
        ref.update({
            'ultrasonico/data': ultrasonico,
            'ultrasonico/time': time1,
            'Temperatura/data': Temperatura,
            'Temperatura/time': time2,

        })
        
        print(ref.get())
        
    except KeyboardInterrupt:
       break
    
    
    
    



