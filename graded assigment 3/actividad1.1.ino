//                                                                                       SEBASTIAN DE LA ROSA - LUISA FERNANDA RAMOS 


const int Trigger = 39;
const int Echo = 34;
const int analogPin = 36;
int pot;

void setup() {
  Serial.begin(115200);
  Serial.println("Bluetooth Device is Ready to Pair");
  pinMode(Trigger, OUTPUT);
  pinMode(Echo, INPUT);
  digitalWrite(Trigger, LOW);
  xTaskCreatePinnedToCore(
      receiveTask1,
      "receiveTask1",
      10000,
      NULL,
      1,
      NULL,0);
  xTaskCreatePinnedToCore(
      receiveTask,
      "receiveTask",
      10000,
      NULL,
      0, 
      NULL,1);
}

void loop() {

}

void receiveTask(void * parameter){                                                                                                   // creamos la funcion para leer los datos  
  for(;;){
    pot = analogRead(analogPin);
    Serial.println("Potenciometro = " + pot);
  }
}

void receiveTask1(void * parameter){                                                                                                      // funsion en el otro nucleo que vamos a ejecutar 
  for(;;){
    long t;
    long d;
  
    digitalWrite(Trigger, HIGH);
    delayMicroseconds(10);
    digitalWrite(Trigger, LOW);
    
    t = pulseIn(Echo, HIGH);
    d = t/59;
    
    Serial.print("Distancia: ");
    Serial.print(d);
    Serial.print("cm");
    Serial.println();
    delay(100); 
  }
}
