#include <WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"
#define DHTPIN 4
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

const char* ssid = "POCO X3 Pro";
const char* password = "felipe123";
//const char* mqttServer = "192.168.1.36";// broker.emqx.io
const char* mqttServer = "broker.emqx.io";
const int mqttPort = 1883;
const char* mqttUser = "emqx";
const char* mqttPassword = "public";
const int portPin = 35;



WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  dht.begin();
  WiFi.begin(ssid, password);
  Serial.println("...................................");

  Serial.print("Connecting to WiFi.");
  while (WiFi.status() != WL_CONNECTED)
       {  delay(500);
          Serial.print(".") ;
       }
  Serial.println("Connected to the WiFi network");

  client.setServer(mqttServer, mqttPort);
  while (!client.connected())
  {      Serial.println("Connecting to MQTT...");
       if (client.connect("ESP32Client", mqttUser, mqttPassword ))
           Serial.println("connected");
       else
       {   Serial.print("failed with state ");
           Serial.print(client.state());
           delay(2000);
       }
}

}

void loop() {
  delay(500);
  // put your main code here, to run repeatedly:
  int Sensor_ins = analogRead(portPin);
  float Sensor2_ins = dht.readTemperature();
  float h = dht.readHumidity();
float t = dht.readTemperature();
  
  client.loop();
     char str[40];
     char str2[40];
     
     if (isnan(h) || isnan(t)) {
Serial.println(F("Failed to read from DHT sensor!"));
return;
}
     sprintf(str, "Sensor1 = %u", Sensor_ins);
     sprintf(str2, "Sensor2 = %6.4f", Sensor2_ins);
     

     client.publish("python/1234", str);
     client.publish("python/1234", str2);
     Serial.println(str);
     Serial.println(str2);
     delay(500);

}
