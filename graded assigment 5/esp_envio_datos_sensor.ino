#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include "max6675.h"
RF24 radio(12, 14, 26, 25, 27);
/* structure that hold data*/

int thermoDO = 19;
int thermoCS = 23;
int thermoCLK = 5;

MAX6675 thermocouple(thermoCLK, thermoCS, thermoDO);

int Pon1 = 16;
float datos[3];
float datos1[3];
typedef struct{
  int sender;
  int datasensor;
  int counter;
}Data;

/* this variable hold queue handle */
xQueueHandle xQueue;
SemaphoreHandle_t xBinarySemaphore;
const byte address[6] = "00020";
void setup() {
  Serial.begin(115200);
  radio.begin();
  
  //set the address
  radio.openWritingPipe(address);
  
  //Set module as transmitter
  radio.stopListening();

   // Starts the serial communication
  Serial.println("");
  ////////////////////////////////////////////////////////////////
 
  ////////////////////////////////////////////////////////////////

  xBinarySemaphore = xSemaphoreCreateBinary();
  /* create the queue which size can contains 5 elements of Data */
  xQueue = xQueueCreate(5, sizeof(Data));
  xTaskCreatePinnedToCore(
      sendTask1,           /* Task function. */
      "sendTask1",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      sendTask2,           /* Task function. */
      "sendTask2",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      2,                        /* priority of the task */
      NULL,0);                    /* Task handle to keep track of created task */
  xTaskCreatePinnedToCore(
      receiveTask,           /* Task function. */
      "receiveTask",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      1,                        /* priority of the task */
      NULL,1);                    /* Task handle to keep track of created task */
}

void loop() {
   // Con analogRead leemos el sensor, recuerda que es un valor de 0 a 1023
  

}

void sendTask1( void * parameter )
{
  /* keep the status of sending data */
  BaseType_t xStatus;
  /* time to block the task until the queue has free space */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  /* create data to send */
  Data data;
  /* sender 1 has id is 1 */
  data.sender = 1;

  for(;;){
    float pot; // Variable para almacenar el valor obtenido del sensor (0 a 1023)
    int pin_pot = 35;
    pot = analogRead(pin_pot); 
   
    // Calculamos la temperatura con la fórmula
    pot = (pot * 100.0)/4095.0; 
    //xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    data.datasensor = pot; 
    datos[0]= pot;

    //Serial.println("sendTask1 is sending data");
    /* send data to front of the queue */
//    Serial.println("\t\t Temperatura en °C - Adding "+String(data.datasensor));
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    /* check whether sending is ok or not */
    if( xStatus == pdPASS ) {
      /* increase counter of sender 1 */
      data.counter = data.counter + 1;
    }
    /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(500));// Simulated interruption
  }
  vTaskDelete( NULL );
}
/* this task is similar to sendTask1 */
void sendTask2( void * parameter )
{
  BaseType_t xStatus;
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  data.sender = 2;

  xSemaphoreGive(xBinarySemaphore);
  
  for(;;){
    float temp;
    temp = thermocouple.readCelsius();
    data.datasensor = temp; 
    datos[1]= temp;
  
    xStatus = xQueueSendToBack( xQueue, &data, xTicksToWait );
    if( xStatus == pdPASS ) {
      data.counter = data.counter + 1;
    }
   /* we delay here so that receiveTask has chance to receive data */
    //xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(pdMS_TO_TICKS(500));// Simulated interruption
  }
  vTaskDelete( NULL );
}
void receiveTask( void * parameter )
{
  
  /* keep the status of receiving data */
  BaseType_t xStatus;
  /* time to block the task until data is available */
  const TickType_t xTicksToWait = pdMS_TO_TICKS(100);
  Data data;
  for(;;){
    /* receive data from the queue */
    xStatus = xQueueReceive( xQueue, &data, xTicksToWait );
    /* check whether receiving is ok or not */

    if(xStatus == pdPASS){
      bool ok = radio.write(datos,sizeof(datos));
    if(ok)
    {
      Serial.print("pot: ");
      Serial.print(datos[0]);
      Serial.println("");
      Serial.print("temp: ");
      Serial.print(datos[1]);
      Serial.println("");
      }
      else{
        Serial.println("Datos no enviados");
        }
      
      
//      //Send message to receiver
//  const char text[] = "Holiiiii";
//  Serial.println("send ...");
//  radio.write(&text, sizeof(text));
     
  
//    Serial.print("Visualizing data: ");
//      Serial.print("Sensor = ");
//      Serial.print(data.sender);
//      Serial.print(" with data = ");
//      Serial.println(data.datasensor);
    
    }
  }
  vTaskDelete( NULL );
}
